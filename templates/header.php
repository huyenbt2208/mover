<div id="page">
<div id="header">
	<div class="con_header nav">
		<div class="container-wrap">
    		<div class="row">
    			<div class="col-sm-2 col-xs-6">
    				<div class="box-logo">
						<h1 class="logo">
	    					<img src="images/logo.png" alt="logo ab"/>
	    				</h1>
					</div>
    				
    			</div>
    			<div class="col-sm-5 col-xs-6">
    				<div class="box-nav visible-md visible-lg visible-sm">
	    				<ul class="nav-left">
	    					<li><a class="active" href="#divteacher">Teacher </a></li>
	    					<li><a href="#divclasses"> classes  </a></li>
	    					<li><a href="#divnews"> news  </a></li>
	    					<li><a href="#divabout">  about us    </a></li>
	    				</ul>
	    			</div>
    				<div id="no1_mommenu" class="menu-offcanvas hidden-md hidden-lg hidden-sm">
                    
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle btn2 btn-navbar offcanvas">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="overlay">
                                    <img class="btn_back" src="<?php echo LOCATION_FILE;?>images/ic_next_red.png" alt="cindi back">
                                </span>
                            </button>
                           
                        </div>                             
                        <div id="menu_offcanvas" class="offcanvas">
                            <ul class="nav-left">
                                <li><a class="active" href="#divteacher">Teacher </a></li>
                                <li><a href="#divclasses"> classes  </a></li>
                                <li><a href="#divnews"> news  </a></li>
                                <li><a href="#divabout">  about us    </a></li>
                                <li><a href="#divabout">  Đăng Nhập    </a></li>
                                <li><a href="#divabout">  Trở Thành Giáo Viên    </a></li>
                            </ul>
                        </div>
                    </div>
    			</div>
    			
    			
    			<div class="col-sm-5 visible-md visible-md visible-lg visible-sm">
                    <div class="box-login">
                        <a class="btn btn-ssm">Đăng Nhập</a>
                        <a class="btn btn-primary">Trở Thành Giáo Viên</a>
                        <div class="box_lang">
                            <a class="lnk_lang dropdown-toggle lang_default" data-toggle="dropdown">&nbsp;</a>
                            <ul class="dropdown-menu animated littleFadeInDown dropdown-language" role="menu">

                                <li class="lag_en">

                                    
                                    <div><img class="btn_back" src="<?php echo LOCATION_FILE;?>images/ic_lan_en.png" alt="cindi language">
                                    <span>EN</span></div>
                                </li>

                                <li class="lag_vn">
                                    <div><img class="btn_back" src="<?php echo LOCATION_FILE;?>images/ic_lang.png" alt="cindi language">
                                    <span>VN</span></div>
                                    
                                </li>
                            </ul>
                        </div>
                        
                        
                    </div>
    				
    				
    			</div>
    			
    		</div>
    	</div>
	</div>	
</div><!-- /#header -->
