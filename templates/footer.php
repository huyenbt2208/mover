<div id="footer">
	<div class="con_footer">
		<div class="container">
			<div class="box_footer">
				<div class="scroll_top_mobile  hidden-md hidden-lg hidden-sm">
				    <a href="#main">
		        		<i class="fas fa-arrow-up"></i>
		        	</a>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-5">
						<div class="footer-introduction"><img src="images/logo_footer.png" alt="">
					        <p class="info">Cindi hiện là nền tảng học IELTS trực tuyến số #1 Việt Nam. Đồng hành cùng chúng tôi, bạn sẽ luyện được những kỹ năng tốt nhất. Với Cindi, việc học ngoại ngữ trở nên nhanh chóng, thuận tiện và dễ dàng hơn.</p>
					        <!-- <h4>FOLLOW US</h4> -->
					        <div class="footer-social-group">
					            <div class="footer-social-item"><a href="#"><i class="fab fa-facebook-f"></i></a></div>
					            <div class="footer-social-item"><a href="#"><i class="fab fa-twitter"></i></a></div>
					            <div class="footer-social-item"><a href="#"><i class="fab fa-instagram"></i></a></div>
					        </div>
					    </div>
					</div>
					<div class="col-xs-12 col-sm-5">
						<div class="footer-contact">
				            <h4>Liên Hệ</h4>
				            <div class="footer-contact-item">
				                <div class="detail-contact">
				                    <div class="contact"><i class="fas fa-map-marker-alt"></i> 107 Nguyễn Phong Sắc, Cầu Giấy, HN</div>
				                </div>
				            </div>
				            <div class="footer-contact-item">
				                <div class="detail-contact">
				                    <div class="contact"><i class="fas fa-phone-volume"></i> (+84) 909 469 666</div>
				                </div>
				            </div>
				            <div class="footer-contact-item">
				                <div class="detail-contact">
				                    <div class="contact"><i class="fa fa-envelope"></i> support@cindi.com</div>
				                </div>
				            </div>
				            
				            
				            
				        </div>
					</div>
					<div class="col-xs-12 col-sm-2">
						<h4>App Download</h4>
						<div class="box_lnk">
							<a href="#1234"><img src="./images/img_appstore.png" alt="" /></a>
							<a href="#1234"><img src="./images/img_googleplay.png" alt="" /></a>
						</div>
					</div>
					
				</div>

			</div>
		</div>
		
		<p class="copyright">© 2019 Cindi. All Rights Reserved.</p>
	</div>
	
</div><!-- /#footer -->
</div><!-- /#page -->
