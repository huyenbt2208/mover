$(function() {
	$('.box_banner').slick({
	  // infinite: true,
	  arrows: false,
	  infinite: true,
	  slidesToShow: 5,
	  // slidesToScroll: 5,
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 5,
	        slidesToScroll: 5,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
	$('.box_list_teacher').slick({
	  // infinite: true,
	  // arrows: false,
	  infinite: true,
	  slidesToShow: 1,
	  speed: 3000,
	  autoplay: false,
	  dots: true,
	  arrows: true,
	  prevArrow: '<i class="fas fa-angle-left"></i>',
	  nextArrow: '<i class="fas fa-angle-right"></i>',
	});
	
	// $('.box_slide_main').slick({
	// 	  // infinite: true,
	// 	  arrows: false,
	// 	  dots: true,
	// 	  infinite: true,
	// 	  speed: 500,
	// 	  fade: true,
	// 	  cssEase: 'linear'
	// 	});

});
$(document).ready(function () {
	$('#no1_mommenu .btn2.offcanvas').on('click', function(){
        if($('#menu_offcanvas').hasClass('active')){
            $(this).find('.overlay').fadeOut(250);
            $('#menu_offcanvas').removeClass('active');
            $('body').removeClass('show-sidebar');
        } else {
            $('#menu_offcanvas').addClass('active');
            $(this).find('.overlay').fadeIn(250);
            $('body').addClass('show-sidebar');
        }
    });
    $("#jsDownloadBtn ").click(function(){
	    $("#jsHeaderDownload").hide();
	    $('#page').addClass('noDowload');
	  });
    $('.dropdown-language').click(function(e) {
    	// alert($(this).getClass().getName());
	    e.stopPropagation();
	});
    $(".dropdown-language li").click(function(){
	    var class_lang = $(this).prop("class");
	    if($(".lnk_lang").hasClass("lag_en")) {
	    	$(".lnk_lang").removeClass("lag_en");
	    }
	    if($(".lnk_lang").hasClass("lag_vn")) {
	    	$(".lnk_lang").removeClass("lag_vn");
	    }
	    
	    $(".lnk_lang").addClass(class_lang);
	    // $(".lnk_lang").attr('class',"test")
	  });
    
});
jQuery(document).ready(function(){
        /*Animate when click anchor link*/
        jQuery('a[href^="#"]').on('click', function(event) {
            var target = jQuery(this.getAttribute('href'));
            if( target.length ) {
                event.preventDefault();
                jQuery('html, body').stop().animate({
                    scrollTop: target.offset().top
                }, 1000);
            }
        });

});